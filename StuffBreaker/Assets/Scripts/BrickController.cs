﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickController : MonoBehaviour
{
    [SerializeField] int HitCount = 2;
    [SerializeField] GameObject ExplosionFX;

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Ball")
        {
            var fx = Instantiate(ExplosionFX, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
