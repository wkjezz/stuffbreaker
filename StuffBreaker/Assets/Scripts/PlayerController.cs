﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float MinPosition = -3;
    [SerializeField] float MaxPosition = 3;
    [SerializeField] float BatSpeed = 3;

    [SerializeField] float MinFireAngle = -45;
    [SerializeField] float MaxFireAngle = 45;
    [SerializeField] float LaunchForce = 200;

    [SerializeField] GameObject ball;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        processTranslation();
        processBallLaunch();
    }

    private void processBallLaunch()
    {
        if(ball == null)
        {
            print("We have no ball");
            return;
        }

        bool fire = CrossPlatformInputManager.GetButton("Fire");

        if (fire)
        {
            var ballController = ball.GetComponent<BallController>();

            float xForce = Random.Range(MinFireAngle, MaxFireAngle);
            float yForce = LaunchForce;


            ballController.LaunchBall(new Vector3(xForce, yForce, 0));
        }
    }

    private void processTranslation()
    {
        float horizontalThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        float xOffset = horizontalThrow * BatSpeed * Time.deltaTime;
        float rawXPosition = transform.localPosition.x + xOffset;
        float yPosition = transform.localPosition.y;
        float zPosition = transform.localPosition.z;

        rawXPosition = Mathf.Clamp(rawXPosition, MinPosition, MaxPosition);

        transform.localPosition = new Vector3(rawXPosition, yPosition, zPosition);
    }
}
