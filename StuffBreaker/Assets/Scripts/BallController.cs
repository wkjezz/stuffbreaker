﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    private bool launched = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LaunchBall(Vector3 direction)
    {
        if (launched)
        {
            return;
        }
        var rigidBody = GetComponent<Rigidbody>();
        if (rigidBody == null)
        {
            print("Could not find rigid body");
        }
        rigidBody.AddForce(direction);
        launched = true;
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Bat")
        {
            var audio = gameObject.GetComponent<AudioSource>();
            audio.Play();
        }
    }
}
